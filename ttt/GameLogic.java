package ttt;
import org.apache.commons.lang3.tuple.*;
import java.util.*;

public class GameLogic {
	Board board;
	private int turn = 0;
	
	public GameLogic() {
		board = new Board(3, 3);
	}
	
	public Pair<Integer, Integer> getInput() {
		Scanner sc = new Scanner(System.in);
		int col;
		int row;
		while (true) {
			System.out.print("Enter in row: ");
			row = sc.nextInt();
			System.out.print("Enter in col : ");
			col = sc.nextInt();
			if (checkIfInputIsValid(col-1, row-1)) {
				System.out.println("d ");
				return new ImmutablePair<>(row-1, col-1);
				// Cannot instantiate the type Pair
			}
		}
	}

	public boolean checkIfInputIsValid(int col, int row) {
		if ((0 <= col && col < board.getNumCols()) && (0 <= row && row < board.getNumRows())) {
			if (board.checkIfPositionValid(row, col)) {
				System.out.println("s");
				return true;
			}
			System.out.println("\n" + "Spot already taken. Enter valid position." + "\n");
			return false;
		}
		System.out.println("\n" + "Enter valid range. " + "\n");
		return false;
	}

	public void runTest() {
		board.randomizeBoard();
		System.out.println(board.printBoard());
		if (board.checkWin(Players.PLAYER_X)) {
			board.printBoard();
			System.out.println("X wins!");
		}
		else if (board.checkWin(Players.PLAYER_Y)) {
			board.printBoard();
			System.out.println("O wins!");
		} else {
			System.out.println("No winner");
		}
		
	}

	public void gameLoop() {
		while(turn < 9) {
			System.out.println(board.printBoard());
			Pair<Integer, Integer> p = getInput();
			System.out.println(p);
			board.changeBoard(p.getLeft(), p.getRight(), turn);
			if (board.checkWin(Players.PLAYER_X)) {
				board.printBoard();
				System.out.println("X wins!");
				break;
			}
			if (board.checkWin(Players.PLAYER_Y)) {
				board.printBoard();
				System.out.println("Y wins!");
				break;
			}
			turn++;
		}
	}

}
