package ttt;
public class Utils {
    
    public static int evenOrOdd(int turn) {
    	if (turn % 2 == 0) {
    		return 1;
    	} else {
    		return 2;
    	}
	}
}
