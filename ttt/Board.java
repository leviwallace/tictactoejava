package ttt;

import java.util.*;

public class Board {
	private List<List<Integer>> b;
	private int numRows;
	private int numCols;

	public Board(int rw, int cl) {
		// construct board using row and col
		// (they are all copies: reason for error)
		numRows = rw;
		numCols = cl;
		b = new ArrayList<>();
		for (int i = 0; i < numRows; i++) {
			List<Integer> row = new ArrayList<>(Collections.nCopies(numCols, 0));
			System.out.println(row);
			b.add(row);
		}
	}

	public String printBoard() {
		// print board in formatted way
		List<String> accum = new ArrayList<>();
		String vert = "----";
		String vertLines = String.join("", Collections.nCopies(numRows, vert));
		accum.add("-" + vertLines + "\n");
		for (int row = 0; row < numRows; row++) {
			accum.add("|");
			for (int col = 0; col < numCols; col++) {
				int player = b.get(row).get(col);
				accum.add(" " + Players.chars[player] + " |");
			}
			accum.add("\n" + "-" + vertLines + "\n");
		}
		return String.join("", accum);
	}

	public void changeBoard(int row, int col, int turn) {
		// change board using input from "input()"
		b.get(row).set(col, Utils.evenOrOdd(turn));
	}

	public boolean checkIfPositionValid(int row, int col) {
		System.out.println(row);
		Integer pos = b.get(row).get(col);
		if (pos == 0) {
			return true;
		}
		return false;
	}
	
	public void randomizeBoard() {
		Random r = new Random();
		for (int row = 0; row < numRows; row++) {
			for (int col = 0; col < numCols; col++) {
				b.get(row).set(col, r.nextInt(3));
			}
		}
	}
	
	private boolean checkRow(int rowNum, int sym) {
		
		for (int value : b.get(rowNum)) {
			if (value != sym)
				return false;
		}
		return true;
	}
	
	
	private boolean checkCol(int columnNumber, int sym) {
		for (int row = 0; row < numRows; row++) {
			if(b.get(row).get(columnNumber) != sym) 
				return false;
		}
		return true;
	}
	
	private boolean checkSlashDiagonal(int sym) {
		for (int row = 0; row < numRows; row++) {
			if (b.get(row).get(numCols-1-row) != sym) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkBackslashDiagonal(int sym) {
		for (int i = 0; i < numRows; i++) {
			if (b.get(i).get(i) != sym) {
				return false;
			}
		}
		return true;
	}
	
	
	
	
	public boolean checkWin(int sym) {
		// sym = either "X", or "O"
		for (int rowNum = 0; rowNum < numRows; rowNum++) {
			if (checkRow(rowNum, sym)) {
				return true;
			}
		}
		for (int columnNumber = 0; columnNumber < numCols; columnNumber++) {
			if (checkCol(columnNumber, sym)) {
				return true;
			}
		}
		if (checkSlashDiagonal(sym)) {
			return true;
		}
		if (checkBackslashDiagonal(sym)) {
			return true;
		}
		return false;
	}
	
	public int getNumRows() {
		return numRows;
	}

	public int getNumCols() {
		return numCols;
	}
}