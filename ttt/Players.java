package ttt;

public class Players {
	
	public static final int BLANK = 0;
	public static final int PLAYER_X = 1;
	public static final int PLAYER_Y = 2;
	
	public static final String[] chars = {" ", "X", "O"};
}